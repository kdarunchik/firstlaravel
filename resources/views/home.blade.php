@extends('layouts.app')

@section('content')
    <div class="alert alert-success msg" id="messageHome" role="alert" style="display: none">
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="card-title one">{{ __('Task lists') }}</h3>
                                <button type="button" class="btn btn-primary float-right" data-toggle="modal"
                                        data-target="#listModal" data-whatever="0">{{ __('Add List') }}
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-list">
                            <thead>
                            <tr>
                                <th style="width: 1%">
                                    #
                                </th>
                                <th>
                                    {{ __('Title') }}
                                </th>
                                <th style="width: 10%" class="text-center">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($taskLists as $taskList)
                                <tr class="list_{{ $taskList['id'] }}">
                                    <td>
                                        {{ $taskList['id'] }}
                                    </td>
                                    <td>
                                        <a href="{{ route('list.view', ['id' => $taskList['id']]) }}"
                                           id="title_{{ $taskList['id'] }}">
                                            {{ $taskList['title'] }}
                                        </a>
                                        <input type="hidden" name="list_notice" id="list_notice_{{ $taskList['id'] }}"
                                               value="{{ $taskList['notice'] }}">
                                        <input type="hidden" name="list_users" id="list_users_{{ $taskList['id'] }}"
                                               value="{{ json_encode($taskList['users_of_list']) }}">
                                    </td>
                                    <td>
                                        @if((Auth::id() == $taskList['user_id']) || (session()->get('user_id') == $taskList['user_id']))
                                            <div class="btn-group align-self-center">
                                                <button type="button" class="btn btn-info" data-toggle="modal"
                                                        data-target="#listModal" data-whatever="{{ $taskList['id'] }}">
                                                    <i class="fas fa-pencil-alt"></i></button>
                                                <button type="button" class="btn btn-danger delete-list"
                                                        value="{{ $taskList['id'] }}"><i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <div class="modal fade" id="listModal" role="dialog" aria-labelledby="listModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="listModalLabel">{{ __('You can create list for tasks here') }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times</span>
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        <form enctype="multipart/form-data" id="myForm">

                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        {{ __('List Title') }}
                                                    </div>
                                                </div>
                                                <input type="text" id="title" name="title"
                                                       class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                                       value="{{ old('title') }}" placeholder="" autofocus>
                                                <div class="invalid-feedback msg" id="errorTitle" style="display: none">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>{{ __('Notice') }}</label>
                                                <textarea class="form-control" id="notice" name="notice" rows="5"
                                                          class="form-control {{ $errors->has('notice') ? 'is-invalid' : '' }}"
                                                          value="{{ old('notice') }}" placeholder=""
                                                          autofocus></textarea>
                                                <div class="invalid-feedback msg" id="errorNotice"
                                                     style="display: none">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>{{ __('Users') }}</label>
                                                <select class="select-users" name="users_id[]" id="users_id" multiple
                                                        class="form-control {{ $errors->has('users_id') ? 'is-invalid' : '' }}"
                                                        style="width: 100%;">
                                                    <option value="" disabled selected>{{ __('Select Users') }}</option>
                                                    @isset($users)
                                                        @foreach($users as $userOfList)
                                                            <option value="{{ $userOfList['id'] }}">
                                                                {{ $userOfList['name'] }}
                                                            </option>
                                                        @endforeach
                                                    @endisset
                                                </select>
                                                <div class="invalid-feedback msg" id="errorUsers" style="display: none">
                                                </div>
                                            </div>

                                            <input type="hidden" name="id" id="list_edit_id" value="">
                                        </form>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}
                                        </button>
                                        <button type="button" type="submit" class="btn btn-primary save-list"
                                                id="submitBtn">{{ __('Save') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
