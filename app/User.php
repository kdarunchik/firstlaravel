<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function lists_by_user()
    {
        return $this->hasMany('App\TaskList', 'user_id', 'id');
    }

    public function lists_for_user()
    {
        return $this->belongsToMany('App\TaskList','list_users', 'user_id', 'list_id')->withTimestamps();
    }

    public static function getAllNotUserAdmin()
    {
        return User::select(['id', 'name'])->where('id', '<>', Auth::id())->get();
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($user) {
            $user->lists_by_user()->each(function($list) {
                $list->delete();
             });
            ListUser::where("user_id", $user->id)->delete();
        });
    }
}
