$(document).ready(function (e) {
    $("#taskForm").on('submit',(function(e) {
        e.preventDefault();
        $('.msg').hide();
        let id = $("#task_edit_id").val();
        const detail = $('#detail').val();
        const authorName = $('#author_name').val();
        let lang = $("html").prop('lang')
        $.ajax({
            type: 'POST',
            url: '/task/store',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                if (data == 'invalid') {
                    let invalFile = ''
                    if (lang == 'en'){
                        invalFile = 'Invalid File!'
                    } else {
                        invalFile = "Файл поврежден!"
                    }
                    $("#errorTask").html(invalFile).fadeIn();
                } else {
                    let msgTask = ''
                    if (lang == 'en'){
                        msgTask = 'Task was saved!'
                    } else {
                        msgTask = "Задача была сохранена!"
                    }
                    $("#messageList").text(msgTask);
                    $('#messageList').show();
                    $('#task_edit_id').val('');
                    if (id == data.id) {
                        $('.task_' + id).remove();
                    }
                    let markup = "<tr class='task_" + id + "'><td><div class='text' id='detail_" + id + "'>" + detail + "</div>";
                    if (data.newFiles) {
                        let attachFile = ''
                        if (lang == 'en'){
                            attachFile = 'Attached files'
                        } else {
                            attachFile = "Прикрепленные файлы"
                        }
                        markup += "<small class='files_" + id + "'>" + attachFile;
                        for (const idFile in data.newFiles) {
                            markup += "<div id='file_" + idFile +"'><a href=\"/file/download/" + idFile +"\" class='btn btn-default btn-sm'><i class='fas fa-file-download'></i></a><button type='button' class='btn btn-default btn-sm delete-file' value='" + idFile +"'><i class='fas fa-trash'></i></button>" + data.newFiles[idFile] +"</div>";
                        }
                        markup += "</small>";
                    }
                    let msgCreatedBy = ''
                    if (lang == 'en'){
                        msgCreatedBy = "Created by "
                    } else {
                        msgCreatedBy = "Создано "
                    }
                    markup += "<small>" + msgCreatedBy + authorName + "</small></td><td><div class='btn-group align-self-center'><button type='button' class='btn btn-info edit-task' value='" + id +"'><i class='fas fa-pencil-alt'></i></button><button type='button' class='btn btn-danger delete-task' value='" + id + "'><i class='fas fa-trash'></i></button></div></td></tr>";
                    $(".table-task tbody").prepend(markup);
                    $("#taskForm")[0].reset();
                }
            },
            error: function (data) {
                if (data.responseJSON.errors.detail) {
                    $('#errorTask').text(data.responseJSON.errors.detail);
                    $('#errorTask').show();
                }
            }
        });
    }));
})

$(document).on('click', ".edit-task", function(e){
    e.preventDefault();
    $('.msg').hide();
    const taskId = $(this).val();
    const taskDetail = $("#detail_" + taskId).text().trim()
    $('#detail').val(taskDetail)
    $('#task_edit_id').val(taskId)
})

$(document).on('click', ".delete-task", function(e){
    e.preventDefault();
    $('.msg').hide();
    const taskId = $(this).val();
    $.ajax({
        type: 'GET',
        url: '/task/delete/' + taskId,
        success: function (data) {
            $('.msg').text(data.success);
            $('.msg').show();
            $('.task_' + taskId).remove();
        }
    });
})

$(document).on('click', ".delete-file", function(e){
    e.preventDefault();
    $('.msg').hide();
    const fileId = $(this).val();
    $.ajax({
        type: 'GET',
        url: '/file/delete/' + fileId,
        success: function (data) {
            $('.msg').text(data.success);
            $('.msg').show();
            if (data.noFilesInTask) {
                const taskId = data.noFilesInTask;
                $('.files_' + taskId).remove();
            } else {
                $('.file_' + fileId).remove();
            }
        }
    });
})
