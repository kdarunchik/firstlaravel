<?php

namespace App\Policies;

use App\{TaskList, User};
use Illuminate\Auth\Access\HandlesAuthorization;

class ListPolicy
{
    use HandlesAuthorization;

    public function view(?User $user, TaskList $list, $userId)
    {
        if (!$user) {
            return $list->user_id == $userId;
        } else {
            return in_array($list->id, $user->lists_for_user->modelKeys());
        }
    }

    public function create(User $user)
    {
        return TRUE;
    }

    public function edit(User $user, TaskList $list)
    {
        return $user->id == $list->user_id;
    }

    public function delete(?User $user, TaskList $list, $userId)
    {
        if (!$user) {
            return $list->user_id == $userId;
        } else {
            return $user->id == $list->user_id;
        }
    }

}
