<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Task extends Model
{
    protected $fillable = [
    	'author_id', 'detail',
    ];

    public function author_task()
    {
        return $this->belongsTo('App\ListUser', 'author_id', 'id');
    }

    public static function getAllListTasks($id)
    {
        $tasks = Task::whereIn('author_id', ListUser::where('list_id', $id)->pluck('id')->toArray())->orderBy('updated_at', 'desc')->with('files')->get();
        foreach ($tasks as $i => $task) {
            $newFiles = [];

            foreach ($task['files']->toArray() as $file) {
                $newFiles[$file['id']] = $file['file_name'];
            }

            $tasks[$i]['files'] = $newFiles;
        }
        return $tasks;
    }

    public function files()
    {
        return $this->hasMany('App\File', 'task_id', 'id');
    }

    public static function boot() {
        parent::boot();
        self::deleting(function($task) {
            $id = $task->id;
            $task->files()->each(function($file) use ($id) {
                $file->delete();
            });
            Storage::deleteDirectory('attachfiles/'.$id);
        });
    }
}
