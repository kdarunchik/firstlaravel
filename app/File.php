<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'task_id', 'attach_path', 'file_name'
    ];

    protected $touches = ['task'];

    public function task()
    {
        return $this->belongsTo('App\Task');
    }

}
