<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['set.locale'])->group(function () {
    Auth::routes();
    Route::middleware(['check.user'])->group(function () {
        Route::get('/', 'TaskListController@index')->name('home');
        Route::prefix('list')->group(function () {
            Route::get('/delete/{id}', 'TaskListController@delete')->name('list.delete');
            Route::post('/store', 'TaskListController@store')->name('list.store');
            Route::get('/{id}', 'TaskListController@view')->name('list.view');
        });
        Route::prefix('task')->group(function () {
            Route::post('/store', 'TaskController@store')->name('task.store');
            Route::get('/delete/{id}', 'TaskController@delete')->name('task.delete');
        });
        Route::prefix('file')->group(function () {
            Route::get('/delete/{id}', 'TaskController@deleteFile')->name('file.delete');
            Route::get('/download/{id}', 'TaskController@downloadFile')->name('file.download');
        });
    });
    Route::get('language/{lang}', function ($lang) {
        Session::put('locale', $lang);
        return back();
    })->name('langroute');
});

