<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskList extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'user_id', 'title', 'notice'
    ];

    public function admin_user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function users_of_list()
    {
        return $this->belongsToMany('App\User','list_users', 'list_id', 'user_id')->withTimestamps();
    }

    public static function getUsersOfList($id)
    {
        return TaskList::find($id)->users_of_list->pluck('name', 'id');
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($list) {
            ListUser::where("list_id", $list->id)->each(function ($userInList) {
                $userInList->delete();
            });
        });
    }

}
