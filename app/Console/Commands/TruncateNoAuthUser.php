<?php

namespace App\Console\Commands;

use App\TaskList;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TruncateNoAuthUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'list:truncate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        TaskList::doesntHave('admin_user')->where('updated_at', '<', Carbon::now()->subDays(2))->each(function ($list) {
            $list->delete();
        });
    }
}
