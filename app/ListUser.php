<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListUser extends Model
{
    protected $fillable = [
        'list_id', 'user_id',
    ];

    protected $touches = ['list'];

    public function list()
    {
        return $this->belongsTo('App\TaskList', 'list_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task', 'author_id', 'id');
    }

    public static function getUserIdInList($list_id, $user_id)
    {
        return ListUser::where("list_id", $list_id)->where("user_id", $user_id)->first()->id;
    }

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($userInList) {
            $userInList->tasks()->each(function ($task) {
                $task->delete();
            });
        });
    }

}
