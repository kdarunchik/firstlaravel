<?php

use App\User;
use Illuminate\Database\Seeder;
use App\TaskList;

class TaskListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        factory(TaskList::class, 30)->create()->each(function ($list) use ($users) {
            $users_id = $users->except($list->user_id)->random(2)->modelKeys();
            $list->users_of_list()->attach(array_merge([$list->user_id], $users_id));
        });
    }
}
