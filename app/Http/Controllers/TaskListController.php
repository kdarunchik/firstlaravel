<?php

namespace App\Http\Controllers;

use App\{User, TaskList, Task, ListUser};
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreListRequest;

class TaskListController extends Controller
{
    public function index()
    {
        $userId = $this->getUserId();
        if (!Auth::check()) {
            $taskLists = TaskList::where('user_id', $userId)->get();
        } else {
            $taskLists = TaskList::whereHas('users_of_list', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })->with('users_of_list:user_id')->get();
            foreach ($taskLists as $i => $list) {
                $taskLists[$i]['users_of_list'] = array_map(function ($user) {
                    return $user['user_id'];
                }, $list['users_of_list']->toArray());
            }
        }
        $users = Auth::id() ? User::getAllNotUserAdmin() : null;
        return view(
            'home',
            [
                "taskLists" => $taskLists,
                "users" => $users,
            ]
        );
    }

    public function view($id)
    {
        $userId = $this->getUserId();
        $list = TaskList::find($id);
        if(empty($list)){
            return redirect()->route('home');
        }
        if ($this->authorize('view', [$list, $userId])) {
            $authorIdInList = ListUser::getUserIdInList($id, $userId);
            $users = TaskList::getUsersOfList($id)->toArray();
            $tasks = Task::getAllListTasks($id);
            $authorName = Auth::user() ? Auth::user()->name : 'guest';
            return view(
                'list',
                [
                    "list" => $list,
                    "users" => $users,
                    "tasks" => $tasks,
                    "authorName" => $authorName,
                    "authorIdInList" => $authorIdInList,
                ]);
        } else {
            return redirect()->route("home");
        }
    }

    public function delete($id)
    {
        $userId = $this->getUserId();
        $list = TaskList::find($id);
        if ($this->authorize('delete', [$list, $userId])) {
            $list->delete();
            return response()->json(['success' => "TaskList # " . $id . " was deleted!"]);
        } else {
            return redirect()->route('home');
        }
        return true;
    }

    public function store(StoreListRequest $request)
    {
        $userId = Auth::id() ?: session()->get('user_id');
    	$taskList = TaskList::updateOrCreate(
            ['id' => $request->id, 'user_id' => $userId],
            ['title' => $request->title, 'notice' => $request->notice]
        );
    	if($request['users_id']){
            $taskList->users_of_list()->sync(array_merge([$taskList->user_id], $request['users_id']));
        } else {
            $taskList->users_of_list()->sync([$taskList->user_id]);
        }
        return response()->json(['id' => $taskList->id]);
    }
}
