<?php

namespace App\Http\Controllers;

use App\Task;
use App\File;
use App\ListUser;
use App\TaskList;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreTaskRequest;
use Illuminate\Support\Facades\Storage;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(StoreTaskRequest $request)
    {
        $task = Task::updateOrCreate(
            ['id' => $request->id, 'author_id' => $request->author_id_in_list],
            ['detail' => $request->detail]
        );
        if($request->attachFiles) {
            $newFiles = [];
            foreach ($request->attachFiles as $file) {
                $path = $file->store('attachfiles/' . $task->id);
                $fileName = $file->getClientOriginalName();
                $createFile = $task->files()->create([
                    'attach_path' => $path,
                    'file_name' => $fileName,
                ]);
                $newFiles[$createFile['id']] = $fileName;
            };
            return response()->json(['id' => $task->id, 'newFiles' => $newFiles]);
        }
        return response()->json(['id' => $task->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $userId = $this->getUserId();
        $task = Task::find($id);
        if ($this->authorize('delete', [$task, $userId])) {
            $task->delete();
            return response()->json(['success' => "Task was deleted!"]);
        } else {
            return redirect()->route('home');
        }
        return true;
    }

    public function deleteFile($id)
    {
        $userId = $this->getUserId();
        $file = File::find($id);
        if ($this->authorize('delete', [$file->task, $userId])) {
            $file->delete();
            Storage::delete($file['attach_path']);
            $noFilesInTask = false;
            if (!Storage::files('attachfiles/' . $file->task->id)) {
                $noFilesInTask = $file->task->id;
                Storage::deleteDirectory('attachfiles/' . $file->task->id);
            };
            return response()->json(['success' => "File was deleted!", 'noFilesInTask' => $noFilesInTask]);
        } else {
            return redirect()->route('home');
        }
    }

    public function downloadFile($id)
    {
        $userId = $this->getUserId();
        $file = File::find($id);
        if(empty($file)){
            return redirect()->route('home');
        }
        if (in_array($file->task->author_task->list_id, TaskList::where('user_id', $userId)->get()->modelKeys())) {
            return Storage::download($file['attach_path'], $file['file_name']);
        } else {
            return redirect()->route('/list/' . $file->task->id);
        }
    }
}
