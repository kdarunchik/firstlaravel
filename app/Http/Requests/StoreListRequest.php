<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'notice' => 'required|min:3'
        ];
        if ($this->id) {
            $rules['title'] = 'required|string|min:3|max:250|unique:task_lists,title,' . $this->id;
        } else {
            $rules['title'] = 'required|string|min:3|max:250|unique:task_lists,title';
        }
        return $rules;
    }
}
