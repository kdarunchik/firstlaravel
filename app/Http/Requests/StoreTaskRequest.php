<?php

namespace App\Http\Requests;

use App\ListUser;
use App\Rules\UniqueTask;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $listId = ListUser::find($this->author_id_in_list)->list_id;
        return
            [
                'detail' => ['required', 'string', 'min:3', 'max:250', new UniqueTask($listId, $this->id)],
            ];
    }
}
