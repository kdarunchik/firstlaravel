<?php

namespace App\Rules;

use App\ListUser;
use App\Task;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class UniqueTask implements Rule
{
    private $listId;
    private $taskId;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($listId, $taskId = null)
    {
        $this->listId = $listId;
        $this->taskId = $taskId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (($this->taskId) && (Task::find($this->taskId)->detail === $value)) {
                return true;
        }
        return !Task::getAllListTasks($this->listId)->where('detail', $value)->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This task is already in the current list.';
    }
}
