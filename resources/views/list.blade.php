@extends('layouts.app')

@section('content')
    <div class="alert alert-success msg" id="messageList" role="alert" style="display: none">
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ __('You can add tasks to list here') }}</h3>
                    </div>

                    <div class="card-body">
                        <strong>{{ __('List Title') }}</strong>
                        <p name="title">
                            {{ $list['title'] }}
                        </p>
                        <hr>

                        <strong>{{ __('Notice') }}</strong>
                        <p>{{ $list['notice'] }}</p>
                        <hr>

                        @if(Auth::check())
                            <strong>{{ __('Users') }}</strong>
                            <ul class="list-group list-group-flush mb-3">
                                @foreach($users as $user)
                                    <li class="list-group-item">{{ $user }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <input type="hidden" name="author_name" id="author_name" value="{{$authorName}}">

                        <table class="table table-task">
                            <thead>
                            <tr>
                                <th>
                                    {{ __('Tasks') }}
                                </th>
                                <th style="width: 10%" class="text-center">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tasks as $task)
                                <tr class="task_{{ $task['id'] }}">

                                    <td>
                                        <div class="text" id="detail_{{ $task['id'] }}">
                                            {{ $task['detail'] }}
                                        </div>
                                        @if($task['files'])
                                            <small class="files_{{ $task['id'] }}">
                                                {{ __('Attached files') }}
                                                @foreach($task['files'] as $idFile => $file)
                                                    <div class="file_{{ $idFile }}">
                                                        <a href="{{ route('file.download', ['id' => $idFile]) }}"
                                                           class="btn btn-default btn-sm"> <i
                                                                class="fas fa-file-download"></i></a>
                                                        @if(($authorIdInList == $task['author_id']))
                                                            <button type="button"
                                                                    class="btn btn-default btn-sm delete-file"
                                                                    value="{{ $idFile }}">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                        @endif
                                                        {{ $file }}
                                                    </div>
                                                @endforeach
                                            </small>
                                        @endif
                                        <small>
                                            {{ __('Created by ') }}
                                            @if(Auth::check())
                                                {{ $task->author_task->user->name }}
                                            @else
                                                {{ $authorName }}
                                            @endif
                                        </small>
                                    </td>

                                    <td>
                                        @if(($authorIdInList == $task['author_id']))
                                            <div class="btn-group align-self-center">
                                                <button type="button" class="btn btn-info edit-task"
                                                        value="{{ $task['id'] }}"><i class="fas fa-pencil-alt"></i>
                                                </button>
                                                <button type="button" class="btn btn-danger delete-task"
                                                        value="{{ $task['id'] }}"><i class="fas fa-trash"></i></button>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer">
                        <form id="taskForm" enctype="multipart/form-data" method="post">
                            <div class="input-group">
                                <input type="text" name="detail" id="detail" placeholder="{{ __("Type Task ...") }}"
                                       class="form-control {{ $errors->has('detail') ? 'is-invalid' : '' }}"
                                       value="{{ old('detail') }}" autofocus>
                                <span class="input-group-append">
                                    <button class="btn btn-primary save-task" type="submit">{{ __('Send') }}</button>
                                </span>
                                <span>
                                    <input type="file" id="attach" name="attachFiles[]" multiple>
                                </span>
                                <div class="invalid-feedback msg" id="errorTask" style="display: none">
                                </div>

                                <input type="hidden" name="author_id_in_list" id="author_id_in_list"
                                       value="{{$authorIdInList}}">
                                <input type="hidden" name="id" id="task_edit_id" value="">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

