<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        'detail' => $faker->realText(50),
        'author_id' => function() {
            return App\ListUser::all()->random()->id;
        }
    ];
});
