<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TaskList;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(TaskList::class, function (Faker $faker) {
    return [
        'title' => $faker->catchPhrase,
        'user_id' => function() {
            return App\User::all()->random()->id;
        },
        'notice' => $faker->realText(100),
    ];
});
