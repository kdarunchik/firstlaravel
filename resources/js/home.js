$('#listModal').on('show.bs.modal', function (event) {
    $('.msg').hide();
    const button = $(event.relatedTarget)
    let lang = $("html").prop('lang')
    if (button.data('whatever') != "0") {
        const listId = button.data('whatever')
        const listTitle = $("#title_" + listId).text().trim()
        const listNotice = $("#list_notice_" + listId).val()
        let usersId = $('#list_users_' + listId).val()
        usersId = JSON.parse(usersId)
        const modal = $(this)
        let editTitle = ''
        if (lang == 'en'){
            editTitle = "You can edit list " + listId + " for tasks here"
        } else {
            editTitle = "Вы можете редактировать список " + listId + " для задач здесь"
        }
        modal.find('.modal-title').text(editTitle)
        modal.find('.modal-body #title').val(listTitle)
        modal.find('.modal-body #notice').val(listNotice)
        modal.find('.modal-body #list_edit_id').val(listId)
        modal.find('.modal-body #users_id').val(usersId)
    } else {
        const modal = $(this);
        let createTitle = ''
        if (lang == 'en'){
            createTitle = 'You can create list for tasks here'
        } else {
            createTitle = "Вы можете создать список для задач здесь"
        }
        modal.find('.modal-title').text(createTitle)
    }
})

$('#listModal').on('hidden.bs.modal', function (e) {
    $(this).find('form').trigger('reset');
})

$(".save-list").click(function(e){
    e.preventDefault();
    $('.msg').hide();
    const title = $("#title").val();
    const notice = $("#notice").val();
    let id = $("#list_edit_id").val();
    const usersId = $('#users_id').val();
    $.ajax({
        type: 'POST',
        url: '/list/store',
        data: {id: id, title: title, notice: notice, users_id: usersId},
        success: function (data) {
            let msgSave = ''
            if (lang == 'en') {
                msgSave = "List #" + data.id + ' ' + title + " was saved!"
            } else {
                msgSave = "Список №" + data.id + ' ' + title + " сохранен!"
            }
            $("#messageHome").text(msgSave);
            $('#messageHome').show();
            if (id == data.id) {
                $('#title_' + id).text(title);
                $("#list_notice_" + id).val(notice);
                $('#list_users_' + id).val(JSON.stringify(usersId));
            } else {
                id = data.id;
                const markup = "<tr class='list_" + id + "'><td>" + id + "</td><td><a href=\"/list/" + id + "\" id='title_" + id + "'>" + title + "</a><input type='hidden' name='list_notice' id='list_notice_" + id + "' value='" + notice + "'><input type=\'hidden\' name=\'list_users\' id=\'list_users_" + id + "\' value=\'" + JSON.stringify(usersId) + "\'></td><td><div class='btn-group align-self-center'><button type='button' class='btn btn-info' data-toggle='modal' data-target='#listModal' data-whatever='" + id +"'><i class='fas fa-pencil-alt'></i></button><button type='button' class='btn btn-danger delete-list' value='" + id + "'><i class='fas fa-trash'></i></button></div></td></tr>"
                $(".table-list tbody").append(markup);
            }
            $('#listModal').modal('hide');
        },
        error: function (data) {
            if (data.responseJSON.errors.title) {
                $('#errorTitle').text(data.responseJSON.errors.title);
                $('#errorTitle').show();
            }
            if (data.responseJSON.errors.notice) {
                $('#errorNotice').text(data.responseJSON.errors.notice);
                $('#errorNotice').show();
            }
        }
    });
})

$(document).on('click', ".delete-list", function(e){
    e.preventDefault();
    $('.msg').hide();
    const listId = $(this).val();
    $.ajax({
        type: 'GET',
        url: '/list/delete/' + listId,
        success: function (data) {
            $('.msg').text(data.success);
            $('.msg').show();
            $('.list_' + listId).remove();
        }
    });
})
